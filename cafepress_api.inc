<?php

class CafePressAPI {

  protected $appKey;

  protected $userToken;

  protected $authType;
  protected $authData = array();

  const PROTOCOL_VERSION = 3;

  const AUTH_ANONYMOUS = 1;
  const AUTH_EMAIL_PASS = 2;

  public function __construct($app_key = NULL) {
    if (!isset($app_key)) {
      $app_key = variable_get('cafepress_api_app_key');
    }

    $this->appKey = $app_key;
  }

  public function setAuthType($type, array $data = array()) {
    $this->authType = $type;
    $this->authData = $data;
    $this->userToken = NULL;
  }

  public function getUserToken() {
    if (!isset($this->userToken)) {
      if (!isset($this->authType)) {
        // Try to load default auth type and data from settings.
        $default_type = variable_get('cafepress_api_auth_type');
        if (!empty($default_type)) {
          $this->setAuthType($default_type, variable_get('cafepress_api_auth_data', array()));
        }
      }

      switch ($this->authType) {
        case self::AUTH_ANONYMOUS:
          // TODO Implement anonymous auth.
          break;

        case self::AUTH_EMAIL_PASS:
          $this->userToken = $this->authenticationGetUserToken($this->authData);
          break;

        default:
          throw new CafePressAPIException(t('Unknown auth type.'));
      }
    }

    return $this->userToken;
  }

  protected function performRequest(CafePressAPIMethod $method, array $data = array(), array $files = array()) {
    // Include the main request class.
    require_once 'HTTP/Request2.php';

    // Build the request.
    $request = new HTTP_Request2($method->buildUrl(), $method->type);
    switch ($method->type) {
      case CafePressAPIMethod::TYPE_GET:
        $url = $request->getUrl();
        $url->setQueryVariables($data);
        break;

      case CafePressAPIMethod::TYPE_POST:
        foreach ($data as $name => $value) {
          $request->addPostParameter($name, $value);
        }

        foreach ($files as $name => $file) {
          /**
           * @var CafePressAPIUploadInterface $file
           */
          $request->addUpload($name, $file->getFile(), $file->getName(), $file->getMimeType());
        }
        break;

      default:
        throw new CafePressAPIException(t('Unknown request type.'));
    }

    // Perform request and check response status.
    $response = $request->send();
    if ($response->getStatus() !== 200) {
      throw new CafePressAPIUnexpectedHTTPResponseException($response);
    }

    return $response->getBody();
  }

  protected function call(CafePressAPIMethod $method, array $data = array(), array $files = array()) {
    // Always add protocol version and app key.
    $data['v'] = self::PROTOCOL_VERSION;
    if (isset($this->appKey)) {
      $data['appKey'] = $this->appKey;
    }
    else {
      throw new CafePressAPIException(t('App key is required.'));
    }

    // Add user token to the call if necessary.
    if (!empty($method->needsUserToken)) {
      $data['userToken'] = $this->getUserToken();
    }

    // Perform the HTTP request.
    $result = $this->performRequest($method, $data, $files);

    // Parse XML and make sure the root element name matches.
    $xml = new SimpleXMLElement($result);
    if (!empty($method->rootElement) && strcasecmp($xml->getName(), $method->rootElement) !== 0) {
      throw new CafePressAPIException(t("Unexpected root element !name for the method !method", array(
        '!name' => $xml->getName(),
        '!method' => $method->method,
      )));
    }

    return $xml;
  }

  public function authenticationGetUserToken(array $params) {
    $method = new CafePressAPIMethod('authentication.getUserToken');
    $method->rootElement = 'value';
    $result = $this->call($method, $params);

    $token = (string) $result;
    if (empty($token)) {
      throw new CafePressAPIException(t('Unable to retrieve the user token.'));
    }

    return $token;
  }

  public function imageUpload($folder, array $files) {
    $method = new CafePressAPIMethod('image.upload');
    $method->needsUserToken = TRUE;
    $method->rootElement = 'values';
    $method->type = CafePressAPIMethod::TYPE_POST;
    $method->host = CafePressAPIMethod::HOST_UPLOAD;
    $data = array(
      'folder' => $folder,
    );
    $result = $this->call($method, $data, $files);

    $ids = array();
    foreach ($result->value as $value) {
      $ids[] = (string) $value;
    }

    return $ids;
  }

  /**
   * Runs template.createSvg method.
   *
   * @param string $url
   *
   * @return SimpleXMLElement
   */
  public function templateCreateSvg($url) {
    $method = new CafePressAPIMethod('template.createSvg');
    $method->rootElement = 'svg';
    $data = array(
      'url' => $url,
    );
    $result = $this->call($method, $data);
    return $result;
  }

  /**
   * Runs design.getExample method.
   *
   * @return SimpleXMLElement
   */
  public function designGetExample() {
    $method = new CafePressAPIMethod('design.getExample');
    $method->rootElement = 'design';
    $result = $this->call($method);
    return $result;
  }

  public function designSave(SimpleXMLElement $design, SimpleXMLElement $svg, $folderName = NULL) {
    $method = new CafePressAPIMethod('design.save');
    $method->type = CafePressAPIMethod::TYPE_POST;
    $method->needsUserToken = TRUE;
    $method->rootElement = 'design';
    $data = array(
      'value' => $design->asXML(),
      'svg' => $svg->asXML(),
    );
    if (isset($folderName)) {
      $data['folderName'] = $folderName;
    }
    $result = $this->call($method, $data);
    return $result;
  }

  public function designMakeFolder($folder_name) {
    $method = new CafePressAPIMethod('design.makeFolder');
    $method->needsUserToken = TRUE;
    $method->rootElement = 'folder';
    $data = array(
      'folderName' => $folder_name,
    );
    $folder = $this->call($method, $data);
    return (string) $folder['id'];
  }

  public function designFindFolderByName($folder_name) {
    $method = new CafePressAPIMethod('design.findFolderByName');
    $method->needsUserToken = TRUE;
    $method->rootElement = 'folder';
    $data = array(
      'name' => $folder_name,
    );
    try {
      $folder = $this->call($method, $data);
      return (string) $folder['id'];
    }
    catch (CafePressAPIUnexpectedHTTPResponseException $e) {
      // Detect "folder not found" this way.
      if ($e->getCode() === 400 && !is_null($e->getCafePressException())) {
        return FALSE;
      }
      else {
        throw $e;
      }
    }
  }

  public function productCreate($merchandise_id) {
    $method = new CafePressAPIMethod('product.create');
    $method->rootElement = 'product';
    $data = array(
      'merchandiseId' => $merchandise_id,
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function productSave(SimpleXMLElement $product) {
    $method = new CafePressAPIMethod('product.save');
    $method->type = CafePressAPIMethod::TYPE_POST;
    $method->needsUserToken = TRUE;
    $method->rootElement = 'product';
    $data = array(
      'value' => $product->asXML(),
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function storeGetExampleStoreSection() {
    $method = new CafePressAPIMethod('store.getExampleStoreSection');
    $method->rootElement = 'storeSection';
    $result = $this->call($method);
    return $result;
  }

  public function storeSaveStoreSection(SimpleXMLElement $section) {
    $method = new CafePressAPIMethod('store.saveStoreSection');
    $method->type = CafePressAPIMethod::TYPE_POST;
    $method->needsUserToken = TRUE;
    $method->rootElement = 'storeSection';
    $data = array(
      'value' => $section->asXML(),
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function productFind($id) {
    $method = new CafePressAPIMethod('product.find');
    $method->rootElement = 'product';
    $data = array(
      'id' => $id,
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function productSaveActive(SimpleXMLElement $product, $is_active) {
    $method = new CafePressAPIMethod('product.saveActive');
    $method->type = CafePressAPIMethod::TYPE_POST;
    $method->needsUserToken = TRUE;
    $method->rootElement = 'product';
    $data = array(
      'value' => $product->asXML(),
      'isActive' => !empty($is_active) ? 'true' : 'false',
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function storeListStores() {
    $method = new CafePressAPIMethod('store.listStores');
    $method->needsUserToken = TRUE;
    $method->rootElement = 'stores';
    $result = $this->call($method);
    return $result;
  }

  public function storeListStoreSections($store_id) {
    $method = new CafePressAPIMethod('store.listStoreSections');
    $method->rootElement = 'storeSections';
    $data = array(
      'storeId' => $store_id,
    );
    $result = $this->call($method, $data);
    return $result;
  }

  public function storeListStoreSubSections($store_id, $parent_id) {
    $method = new CafePressAPIMethod('store.listStoreSubSections');
    $method->rootElement = 'storeSections';
    $data = array(
      'storeId' => $store_id,
      'storeSectionid' => $parent_id,
    );
    $result = $this->call($method, $data);
    return $result;
  }

}

class CafePressAPIMethod {

  const TYPE_GET = 'GET';
  const TYPE_POST = 'POST';

  const HOST_DEFAULT = 'api.cafepress.com';
  const HOST_UPLOAD = 'upload.cafepress.com';

  const SCHEMA_HTTP = 'http';

  public function __construct($method) {
    $this->method = $method;
  }

  public $method = '';

  public $type = self::TYPE_GET;

  public $schema = self::SCHEMA_HTTP;

  public $host = self::HOST_DEFAULT;

  public $needsUserToken = FALSE;

  public $rootElement = '';

  public function buildUrl() {
    $url = "{$this->schema}://{$this->host}/{$this->method}.cp";

    return $url;
  }

}

interface CafePressAPIUploadInterface {

  public function getMimeType();

  public function getName();

  public function getFile();

}

class CafePressAPIUploadFile implements CafePressAPIUploadInterface {

  protected $filename;

  protected $mime;

  public function __construct($filename, $mime = NULL) {
    $this->filename = $filename;
    $this->mime = $mime;
  }

  public function getMimeType() {
    return $this->mime;
  }

  public function getName() {
    return basename($this->filename);
  }

  public function getFile() {
    return $this->filename;
  }

}

class CafePressAPIUploadFileManaged implements CafePressAPIUploadInterface {

  protected $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function getMimeType() {
    return $this->file->filemime;
  }

  public function getName() {
    return $this->file->filename;
  }

  public function getFile() {
    return $this->file->uri;
  }

}

class CafePressAPIException extends Exception {}

class CafePressAPIUnexpectedHTTPResponseException extends CafePressAPIException {

  protected $cafePressException;

  public function getCafePressException() {
    return $this->cafePressException;
  }

  public function __construct(HTTP_Request2_Response $response, $message = "") {
    if ($message !== '') {
      $message .= ' ';
    }

    $message .= t('Unexpected response from the API: !code !reason. Effective request URL: !url', array(
      '!code' => $response->getStatus(),
      '!reason' => $response->getReasonPhrase(),
      '!url' => $response->getEffectiveUrl(),
    ));

    // Try to decode the exception if result is a valid XML.
    $xml_types = array('application/xml', 'text/xml');
    if (in_array($response->getHeader('Content-Type'), $xml_types, TRUE)) {
      $body = $response->getBody();
      if (!empty($body)) {
        try {
          $xml = @new SimpleXMLElement($body);
          if (!empty($xml->{'exception-message'})) {
            $this->cafePressException = (string) $xml->{'exception-message'};
            $message .= t(' Exception returned: !exception', array(
              '!exception' => $this->cafePressException,
            ));
          }
        }
        catch (Exception $e) {
          // Just suppress the exception.
        }
      }
    }

    parent::__construct($message, $response->getStatus());
  }

}
