<?php

function cafepress_api_settings_form($form, &$state) {
  $key = variable_get('cafepress_api_app_key');

  $form['key'] = array(
    '#type' => 'fieldset',
    '#title' => t('API key'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($key),
  );
  $form['key']['cafepress_api_app_key'] = array(
    '#required' => TRUE,
    '#type' => 'textfield',
    '#title' => t('CafePress API key'),
    '#default_value' => $key,
  );

  $form['auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default user'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($key),
  );
  $form['auth']['cafepress_api_auth_type'] = array(
    '#type' => 'select',
    '#title' => t('Authentication type'),
    '#options' => array(
      0 => t('No defaults'),
      CafePressAPI::AUTH_ANONYMOUS => t('Anonymous'),
      CafePressAPI::AUTH_EMAIL_PASS => t('Email and password'),
    ),
    '#default_value' => variable_get('cafepress_api_auth_type'),
  );

  $form['auth']['cafepress_api_auth_data'] = array(
    '#tree' => TRUE,
  );

  $auth_data = variable_get('cafepress_api_auth_data', array());
  $states = array(
    'visible' => array(
      'select[name="cafepress_api_auth_type"]' => array(
        'value' => CafePressAPI::AUTH_EMAIL_PASS,
      ),
    ),
  );
  $form['auth']['cafepress_api_auth_data']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => isset($auth_data['email']) ? $auth_data['email'] : '',
    '#states' => $states,
  );
  $form['auth']['cafepress_api_auth_data']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#states' => $states,
  );

  if (isset($auth_data['password'])) {
    $form['auth']['cafepress_api_auth_data']['password']['#description'] = t('Enter only if you want to change current saved password.');
  }

  return system_settings_form($form);
}

function cafepress_api_settings_form_validate($form, &$state) {
  $values = $state['values'];
  $api = new CafePressAPI($values['cafepress_api_app_key']);

  // TODO Try API key somehow.

  $auth_type = $values['cafepress_api_auth_type'];
  if (empty($auth_type) || $auth_type == CafePressAPI::AUTH_ANONYMOUS) {
    // No defaults or anonymous selected. Cleanup auth data.
    $state['values']['cafepress_api_auth_data'] = array();
  }
  elseif ($auth_type == CafePressAPI::AUTH_EMAIL_PASS) {
    $new_data = $values['cafepress_api_auth_data'];
    $saved_data = variable_get('cafepress_api_auth_data', array());

    // Verify email. Don't validate email address to avoid problems, because we
    // don't know how CafePress email validation works.
    if (empty($new_data['email'])) {
      form_set_error('cafepress_api_auth_data][email', t('Email is required.'));
      return;
    }

    // Make sure there is a password entered or saved.
    if (!isset($new_data['password']) || $new_data['password'] === '') {
      if (isset($saved_data['password'])) {
        // Set old password as a value to save it again.
        $state['values']['cafepress_api_auth_data']['password'] = $saved_data['password'];
        $new_data['password'] = $saved_data['password'];
      }
      else {
        form_set_error('cafepress_api_auth_data][password', t('Password is required.'));
        return;
      }
    }

    // Try to sign in.
    try {
      $api->authenticationGetUserToken($new_data);
    }
    catch (Exception $e) {
      watchdog_exception('cafepress_api', $e);
      form_set_error('cafepress_api_auth_data', t('Unable to sign in using entered email and password.'));
    }
  }
}
